#!/usr/bin/env bash
set -o errexit
set -o nounset
set -o pipefail
IFS=$'\n\t'

stow_pkg="$1"

(cd "$CODEPATH/dotfiles/" || exit

  cp ~/.config/mimeapps.list "./$stow_pkg/.config/"
  rm ~/.config/mimeapps.list

  stow -t ~ mime

)

