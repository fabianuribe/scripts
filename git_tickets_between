#!/usr/bin/env python3

""".

Given a prefix, it finds the latest tag with that prefix and
the tag that came before it.

It tries to extract the Jira ticket keys from the messages of
the commits between those two tags.

"""

import argparse
import pprint
import subprocess
import re

def get_commit_messages_between(start_tag: str, end_tag: str) -> list[str]:
    """Get commits between start_tag and end_tag"""
    git_log_command = f"git log {start_tag}..{end_tag} --pretty=format:'%s'"
    output = subprocess.check_output(git_log_command, shell=True, text=True)

    return output.split("\n")


def extract_ticket_keys(messages: list[str]) -> set[str]:
    """Extract ticket keys from messages"""
    tickets = set()
    for message in messages:
        matches = re.findall(r"\b[A-Z]{1,10}-\d+\b", message)
        if len(matches) >= 1:
            tickets.update(matches)
        else:
            # ignore commit message if we can't find the ticket
            pass

    return tickets


def get_version_tags(prefix: str) -> tuple[str, str]:
    """
    Compute the start_tag and end_tag

      - latest_tag : largest version that matches prefix
      - previous_tag : version before this_tag
    """
    latest_tag_command = f"git describe --tags --abbrev=0 --match '{prefix}.*'"
    latest_tag = subprocess.check_output(
        latest_tag_command, shell=True, text=True
    ).strip()
    previous_tag_command = (
        f"git describe --tags --abbrev=0 --match '{args.prefix}.*' {latest_tag}^"
    )
    previous_tag = subprocess.check_output(
        previous_tag_command, shell=True, text=True
    ).strip()

    return (previous_tag, latest_tag)

### Automatically find "previous" tag ####
# if __name__ == "__main__":
#     parser = argparse.ArgumentParser(
#         description="Extract commit messages from a Git repository"
#     )
#     parser.add_argument("prefix", nargs="?", help="prefix to extract Git log for")
#     args = parser.parse_args()

#     tags = get_version_tags(args.prefix)
#     print("Tags: ", tags)
#     (previous_tag, latest_tag) = tags

#     commit_messages = get_commit_messages_between(previous_tag, latest_tag)
#     # print(commit_messages)

#     tickets = extract_ticket_keys(commit_messages)
#     print("Tickets:")
#     pprint.pprint(tickets)

### Manually specify start and end tag ####
if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Extract commit messages from a Git repository"
    )
    parser.add_argument("start_tag")
    parser.add_argument("end_tag")
    args = parser.parse_args()

    commit_messages = get_commit_messages_between(args.start_tag, args.end_tag)

    tickets = extract_ticket_keys(commit_messages)
    print("Tickets:")
    pprint.pprint(tickets)
