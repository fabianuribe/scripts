#!/usr/bin/env python3

import argparse
from dataclasses import dataclass
from datetime import date, datetime
from typing import List, Optional, Set

from rich import pretty, print

pretty.install()


@dataclass
class Todo:
    is_completed: Optional[bool]
    priority: Optional[str]
    completion_date: Optional[date]
    creation_date: Optional[date]
    due_date: Optional[date]
    threshold_date: Optional[date]
    context: Set[str]
    project: Set[str]
    words: List[str]


def parse_completion(part: str) -> bool:
    return part == "x"


def parse_date(part: str) -> Optional[date]:
    try:
        return datetime.strptime(part, "%Y-%m-%d").date()
    except ValueError:
        return None


def parse_priority(part: str) -> Optional[str]:
    if part.startswith("(") and part.endswith(")"):
        priority = part[1].upper()
        return priority
    else:
        return None


def parse_date_tag(tag: str, part: str) -> Optional[date]:
    if part.startswith(f"{tag}:"):
        # e.g. tag='t'; we parse everything after 'len(tag) + 1'
        part = part[len(tag) + 1 :]
        date = parse_date(part)
        return date


def parse_todo_line(line: str) -> Todo:
    is_completed = False
    priority = None
    completion_date = None
    creation_date = None
    due_date = None
    threshold_date = None
    context = set()
    project = set()
    words = []

    parts = line.strip().split()

    def advance():
        "Advances the parse"
        nonlocal parts
        parts = parts[1:]

    def parse_optional(parse_fn, part):
        "Parses `part` with parse_fn. If successful, advances"
        x = parse_fn(part)
        if x:
            advance()
        return x

    # completion & date
    if parts:
        is_completed = parse_optional(parse_completion, parts[0])
        if is_completed:
            completion_date = parse_optional(parse_date, parts[0])

    # priority
    if parts:
        priority = parse_optional(parse_priority, parts[0])

    # creation date
    if parts:
        creation_date = parse_optional(parse_date, parts[0])

    # words, contexts, projects, ...
    for part in parts:
        due_date = parse_date_tag("due", part)
        if due_date:
            continue

        threshold_date = parse_date_tag("t", part)
        if threshold_date:
            continue

        # context
        if part.startswith("@"):
            context.add(part[1:])
        # project
        elif part.startswith("+"):
            project.add(part[1:])
        # words
        else:
            words.append(part)

    return Todo(
        is_completed=is_completed,
        priority=priority,
        completion_date=completion_date,
        creation_date=creation_date,
        due_date=due_date,
        threshold_date=threshold_date,
        context=context,
        project=project,
        words=words,
    )


def parse_todo_file(file_path: str) -> List[Todo]:
    todos = []
    with open(file_path, "r") as file:
        for line in file:
            line = line.lower().strip()
            if line:
                todo = parse_todo_line(line)
                todos.append(todo)
    return todos


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("filename", help="path to todo.txt file")
    args = parser.parse_args()

    todos = parse_todo_file(args.filename)

    # select all the body (words) of todos that have any of the CONTEXTS
    # CONTEXTS = {"tesco"}
    # todos = {
    #     " ".join(todo.words)
    #     for todo in todos
    #     if todo.words and CONTEXTS.intersection(todo.context)
    # }

    # # sort 'em
    # todos = list(todos)
    # todos.sort()

    # print 'em
    print(todos)
